# intro

- app check that voting is still on from DB `closed: false`
  - if `closed:true` switch view and show the highest->lowest voted images
  - disable voting part
- people can send the votes
- 3 votes per person
- manually set the closed to true

# Setup env

```sh
# get the mongodb
docker-compose up -d

#instal deps
yarn
yarn boot

# start the app and the server
yarn start

```

After all is up and running access the
http://localhost:8081/ and create the DB named `tcc`

then paste http://localhost:4567/process_photos in the browser. that will generate the exif and save the photos to the DB

then past to the browser http://localhost:4567/start

Api endpoints

get ALL the photos http://localhost:4567/photos
create a vote ( naive approach ) http://localhost:4567/vote/PHOTO._id

docker build -t woss/amelia-tcc-app:latest -f Dockerfile .
docker push woss/amelia-tcc-app:latest

docker build -t woss/amelia-tcc-api:latest -f Dockerfile .
docker push woss/amelia-tcc-api:latest
