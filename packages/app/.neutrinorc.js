const standard = require('@neutrinojs/standardjs')
const react = require('@neutrinojs/react')
const jest = require('@neutrinojs/jest')

module.exports = {
  options: {
    root: __dirname
  },
  use: [
    standard(),
    react({
      html: {
        template: './index.ejs',
        title: 'The Confinement Chronicle',
        appMountId: 'root',
        lang: 'en',
        meta: {
          viewport: 'width=device-width, initial-scale=1'
        },
        // Override pluginId to add an additional html-template plugin instance
        pluginId: 'html',
        // Add additional Babel plugins, presets, or env options
        babel: {
          // Override options for @babel/preset-env, showing defaults:
          presets: [
            [
              '@babel/preset-env',
              {
                // Targets the version of Node.js used to run webpack.
                targets: { node: 'current' },
                useBuiltIns: 'usage'
              }
            ]
          ]
        }
      }
    }),
    jest()
  ]
}
