import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { Button, Card, Header } from 'semantic-ui-react'
import { ADMIN_KEY, API_URL } from './config'
import GalleryImage from './Gallery/GalleryImage'

export function Admin (props) {
  const history = useHistory()
  const { key } = useParams()

  const [settings, setSettings] = useState()
  const [stats, setStats] = useState([])

  if (key !== ADMIN_KEY) {
    history.replace('/')
  }
  async function testApi () {
    const { data: settingsData } = await Axios.get(`${API_URL}/settings`)
    setSettings(settingsData)
    const { data: statsData } = await Axios.get(`${API_URL}/stats`)
    setStats(statsData)
  }

  useEffect(() => {
    testApi()
  }, [])

  async function selectWinner (imageId) {
    try {
      await Axios.post(`${API_URL}/choose_winner`, {
        artId: imageId
      })
      await testApi()
    } catch (error) {}
  }

  return (
    <div className='margin-14-top'>
      <Header>Admin section</Header>
      <Button onClick={() => history.push('/')}>Home</Button>
      <div>
        <Header>List of sorted by amount of votes</Header>
        <Card.Group itemsPerRow={3} doubling stackable>
          {stats.map(image => (
            <GalleryImage
              key={image._id}
              imageId={image._id}
              votingEnabled={false}
              settings={settings}
              admin
              selectWinner={selectWinner}
            />
          ))}
        </Card.Group>
      </div>
    </div>
  )
}
