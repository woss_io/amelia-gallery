/* eslint react/prop-types: 0 */

import Axios from 'axios'
import { nanoid } from 'nanoid'
import React, { useEffect, useState } from 'react'
import { hot } from 'react-hot-loader'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import 'semantic-ui-css/semantic.min.css'
import {
  Container,
  Divider,
  Header,
  Icon,
  Image,
  Segment
} from 'semantic-ui-react'
import flashGalleryLogo from '../assets/logo-flash-gallery.jpg'
import frikifishLogo from '../assets/logo-frikifish.png'
import sensioLogo from '../assets/logo-sensio-group.svg'
import { Admin } from './Admin'
import './App.css'
import { API_URL } from './config'
import Art from './Gallery/Art'
import Gallery from './Gallery/Gallery'

const message = 'The Confinement Chronicle'
const subtitleMessage =
  'International artists provide perspective, comfort and hope in times of uncertainty.'

function App () {
  const defaultSettings = {
    start: true,
    votingEnabled: true
  }

  const defaultUser = JSON.parse(global.localStorage.getItem('user')) || {
    id: null,
    votesLeft: null
  }
  const [settings, setSettings] = useState(defaultSettings)
  const [user, setUser] = useState(defaultUser)

  useEffect(() => {
    async function getSettings () {
      const response = await Axios.get(`${API_URL}/settings`)
      const s = response.data
      setSettings(s)
    }

    function checkForUserId () {
      if (!user.id) {
        const _usr = {
          id: nanoid(),
          votesLeft: 3
        }
        global.localStorage.setItem('user', JSON.stringify(_usr))
        setUser(_usr)
      }
    }

    getSettings()
    checkForUserId()
    setInterval(() => {
      const _usr = JSON.parse(global.localStorage.getItem('user'))
      if (_usr.votesLeft !== user.votesLeft) {
        setUser(_usr)
      }
    }, 100)
  }, [user.id, user.votesLeft])

  return (
    <Router>
      {user.votesLeft !== 0 && (
        <Segment className='floatingVoteMessage' circular>
          <Icon name='heart' color='red' />
          {user.votesLeft} Votes left
        </Segment>
      )}
      <Container>
        <Switch>
          <Route path='/admin/:key'>
            <Admin settings={settings} user={user} />
          </Route>
          <Route path='/art/:id'>
            <Art settings={settings} user={user} />
          </Route>
          <Route path='/'>
            <Home settings={settings} user={user} />
          </Route>
        </Switch>
      </Container>
    </Router>
  )
}

function Home (props) {
  return (
    <div className='homeWrapper'>
      <Segment className='headingWrapper'>
        <Header as='h1' content={message} className='headingTitle' />
        <Header as='h3' content={subtitleMessage} className='headingSubTitle' />
      </Segment>
      <Paragraph />
      <Gallery {...props} />
      <br />
      <Divider />
      <EndParagraph />
      <Footer />
    </div>
  )
}

const Paragraph = () => (
  <Segment className='welcomeMessage'>
    <p>
      On April 1, 2020, at the start of the COVID-19 confinement, FrikiFish
      launched a global open call offering an opportunity for artists to reflect
      on the current economic, political, and social situation through the arts.
      We asked artists to reflect on the following themes: confinement,
      pandemic, isolation, resilience, social distancing, camaraderie, catalyst
      for change and a new paradigm.
    </p>
    <p>
      <strong>The final online exhibition consists of 30 finalists</strong>
      <br />
      With the help of generous contributors and collaborators:{' '}
      <a href='https://sensio.group' target='_blank' rel='noopener noreferrer'>
        Sensio Group
      </a>
      ,{' '}
      <a
        href='https://flashgallerybcn.com/'
        target='_blank'
        rel='noopener noreferrer'
      >
        Flash Gallery BCN
      </a>{' '}
      and the{' '}
      <a
        href='https://frikifish.com/'
        target='_blank'
        rel='noopener noreferrer'
      >
        FrikiFish
      </a>{' '}
      community, we selected a talented group of 30 artists and raised a cash
      prize of <strong>€500</strong>. Now we’d like to ask for your
      participation!
    </p>
    <p>
      <strong>Vote for your 3 favorite artists</strong>
      <br />
      You have 3 votes to give away to three different artists. On August 15th
      we will announce the winner of the Confinement Chronicle cash prize.
    </p>
  </Segment>
)
const EndParagraph = () => (
  <Segment className='endMessage margin-14-bottom'>
    <p>
      If you’d like to see the complete collection of submissions for the online
      exhibition, please visit{' '}
      <a
        href='https://www.instagram.com/explore/tags/theconfinementchronicle/'
        target='_blank'
        rel='noopener noreferrer'
      >
        #TheConfinementChronicle
      </a>{' '}
      on Instagram.
    </p>
    <p>
      <strong>Thanks to our collaborators!</strong>
      <br />
      We could not have put this online exhibition together without the generous
      contributions from the following groups:
    </p>
    <p style={{ fontSize: '1.3em' }}>
      <a href='http://sensio.group' target='_blank' rel='noopener noreferrer'>
        Sensio Group
      </a>
      <br />
      <i>Cash prize sponsor, developer and host for the online exhibition.</i>
      <br />
      Sensio builds open-source software for content creators, mainly
      photographers, helping them to protect and manage their work online.
      Please follow them on{' '}
      <a
        href='https://twitter.com/sensio_group'
        target='_blank'
        rel='noopener noreferrer'
      >
        Twitter
      </a>{' '}
      if you want to stay posted about the project's developments.
    </p>
    <p>
      <a
        href='https://flashgallerybcn.com/'
        target='_blank'
        rel='noopener noreferrer'
      >
        Flash Gallery BCN
      </a>
      <br />
      <i>Art curator for the online exhibition</i>
      <br />
      Flash Gallery is a non-profit institution providing a unique space in the
      Sants district of Barcelona to exhibit artwork from selected international
      artists. The Gallery will soon be opening a second location to expand
      their support and exposure for artists.
    </p>
    <p>
      <a
        href='https://frikifish.com/'
        target='_blank'
        rel='noopener noreferrer'
      >
        FrikiFish
      </a>
      <br />
      <i>Organizer and sponsors for the online exhibition</i>
      <br />
      I’d like to thank all of the FrikiFish friends and community who
      contributed what they could during these difficult times to our
      crowdfunding campaign. One of our goals was to support artists and
      creatives who cannot make an income during the current global pandemic.
      We’re very proud to offer a 500€ cash prize to the selected artist from
      this group!
    </p>
    <p>
      <strong>
        And last but not least, thanks to everyone who enjoys viewing the online
        exhibition and votes for their favorite artists!
      </strong>
    </p>
  </Segment>
)

function Footer () {
  return (
    <div>
      <Image.Group size='small' className='footerLogos'>
        <Image style={{ height: 80 }} src={frikifishLogo} />
        <Image style={{ height: 80 }} src={sensioLogo} />
        <Image style={{ height: 80 }} src={flashGalleryLogo} />
      </Image.Group>
      <div className='margin-14-bottom' />
    </div>
  )
}
export default hot(module)(App)
