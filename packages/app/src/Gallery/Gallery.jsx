/* eslint react/prop-types: 0 */

import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Card } from 'semantic-ui-react'
import { API_URL } from '../config'
import GalleryImage from './GalleryImage'

export default function Gallery ({ settings, user }) {
  const [images, setImages] = useState([])
  useEffect(() => {
    async function testApi () {
      const { data } = await axios.get(`${API_URL}/photos`)

      if (data) {
        setImages(data)
        global.localStorage.setItem(
          'photos',
          JSON.stringify(data.map(p => p._id))
        )
      }
    }
    testApi()
  }, [])
  return (
    <div className='gallery'>
      <Card.Group
        itemsPerRow={3}
        doubling
        stackable
        // style={{
        //   display: 'flex',
        //   flexWrap: 'wrap',

        //   justifyContent: 'space-evenly'
        // }}
      >
        {images.map(image => (
          <GalleryImage
            key={image._id}
            imageId={image._id}
            votingEnabled={settings.votingEnabled}
            settings={settings}
            user={user}
          />
        ))}
      </Card.Group>
    </div>
  )
}
