import qs from 'querystring'

const defaultQsParams = {
  auto: 'format',
  fit: 'crop'
}

export const createImageSrc = ({ path, sizes }) => {
  if (!path) {
    return null
  }
  sizes = sizes || {}
  const url = `${path}?${qs.stringify({
    ...defaultQsParams,
    ...sizes
  })}`

  return url
}
/**
 *  Create img srcSet
 * @param {*} param0
 */
export const createImageSrcSet = ({ path, square }) => {
  const src = createImageSrc({ path })

  const srcset = [320, 768].map(width => {
    const height = square ? { h: width } : {}
    const myParams = Object.assign(
      {},
      defaultQsParams,
      {
        w: width
      },
      height
    )
    return src + `&${qs.stringify(myParams)} ${width}w`
  })

  return srcset
}
