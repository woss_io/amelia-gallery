/* eslint react/prop-types: 0 */

import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import {
  Button,
  Card,
  Icon,
  Image,
  Label,
  Message,
  Placeholder
} from 'semantic-ui-react'
import { API_URL } from '../config'
import { createImageSrc, createImageSrcSet } from './helpers'

export default function GalleryImage ({
  imageId,
  votingEnabled,
  user,
  admin,
  settings,
  selectWinner
}) {
  const history = useHistory()
  const defaultImage = {
    _id: '',
    title: '',
    artist: '',
    copyright: '',
    desc: '',
    url: null,
    votes: 0,
    voters: [],
    comment: ''
  }
  const [image, setImage] = useState(defaultImage)
  const [loading, setLoading] = useState(true)
  const [errorMsg, setErrorMsg] = useState()
  const [successMsg, setSuccessMsg] = useState()

  useEffect(() => {
    async function testApi () {
      const { data } = await Axios.get(`${API_URL}/art/${imageId}`)
      setImage(data)
      setLoading(false)
    }
    testApi()
  }, [imageId])

  async function handleVote () {
    if (user.votesLeft === 0) {
      setErrorMsg('We are sorry, but you voted already three times')
      return
    }
    const payload = {
      artId: image._id,
      userId: user.id
    }
    try {
      const { data } = await Axios.post(`${API_URL}/vote`, payload)
      if (!data.success) {
        setErrorMsg(data.message)
        setTimeout(() => setErrorMsg(''), 3000)
      } else {
        setSuccessMsg('Thanks for your vote!')
        global.localStorage.setItem(
          'user',
          JSON.stringify({ ...user, votesLeft: user.votesLeft - 1 })
        )
        setTimeout(() => setSuccessMsg(''), 3000)
        setImage(data.art)
      }
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <Card fluid className='galleryImage'>
      {errorMsg && (
        <Message hidden={!errorMsg} className='floatingMessage' negative>
          <Message.Header>{errorMsg}</Message.Header>
        </Message>
      )}
      {successMsg && (
        <Message hidden={!successMsg} className='floatingMessage' positive>
          <Message.Header>{successMsg}</Message.Header>
        </Message>
      )}
      {loading ? (
        <Placeholder>
          <Placeholder.Image square />
        </Placeholder>
      ) : (
        <Image
          className='imageContainer'
          src={createImageSrc({ path: image.url, sizes: { w: 300, h: 300 } })}
          srcSet={createImageSrcSet({ path: image.url, square: true })}
          wrapped
          onClick={() => history.push(`/art/${image._id}`)}
        />
      )}

      <Card.Content>
        {loading ? (
          <Placeholder>
            <Placeholder.Header>
              <Placeholder.Line length='very short' />
              <Placeholder.Line length='medium' />
            </Placeholder.Header>
            <Placeholder.Paragraph>
              <Placeholder.Line length='short' />
            </Placeholder.Paragraph>
          </Placeholder>
        ) : (
          <>
            <Card.Header>
              {settings.winnerIs === image._id
                ? `WINNER!!!  ${image.title}`
                : image.title}
            </Card.Header>
            <Card.Meta>©{image.copyright}</Card.Meta>
          </>
        )}
      </Card.Content>
      <Card.Content extra className='extraActions'>
        {!admin && (
          <Button.Group widths='3'>
            <Button disabled={loading} as='div' labelPosition='right'>
              <Button
                disabled={
                  !votingEnabled ||
                  image.voters.includes(user.id) ||
                  user.votesLeft === 0
                }
                color='violet'
                onClick={handleVote}
              >
                <Icon name='heart' color='red' />

                {votingEnabled ? 'Vote' : 'Voting ended'}
              </Button>
              {!votingEnabled && (
                <Label as='a' basic color='violet' pointing='left'>
                  {image.votes} vote{image.votes !== 1 ? 's' : ''}
                </Label>
              )}
            </Button>
            {votingEnabled && (
              <>
                <Button.Or />
                <Button
                  disabled={loading}
                  color='violet'
                  onClick={() => history.push(`/art/${image._id}`)}
                  animated
                >
                  <Button.Content visible>Learn more</Button.Content>
                  <Button.Content hidden>
                    <Icon name='arrow right' />
                  </Button.Content>
                </Button>
              </>
            )}
          </Button.Group>
        )}
        {admin && (
          <Button disabled={settings.winnerIs} as='div' labelPosition='right'>
            <Button color='violet' onClick={() => selectWinner(image._id)}>
              <Icon name='winner' />
              Select as a Winner
            </Button>
            {!votingEnabled && (
              <Label as='a' basic color='violet' pointing='left'>
                {image.votes} - Total votes
              </Label>
            )}
          </Button>
        )}
      </Card.Content>
    </Card>
  )
}
