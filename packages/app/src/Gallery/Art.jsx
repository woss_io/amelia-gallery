/* eslint react/prop-types: 0 */

import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import {
  Button,
  Dimmer,
  Header,
  Icon,
  Image,
  Loader,
  Message
} from 'semantic-ui-react'
import { API_URL } from '../config'
import { createImageSrc, createImageSrcSet } from './helpers'

export default function Art ({ settings, user }) {
  const { votingEnabled } = settings
  const history = useHistory()

  const [loading, setLoading] = useState(true)
  const [errorMsg, setErrorMsg] = useState()
  const [successMsg, setSuccessMsg] = useState()
  // setTimeout(() => {
  //   setLoading(false)
  // }, 700)
  const defaultImage = {
    _id: '',
    title: '',
    artist: '',
    copyright: '',
    desc: '',
    url: null,
    votes: 0,
    voters: [],
    comment: ''
  }
  const [image, setImage] = useState(defaultImage)

  const { id } = useParams()

  useEffect(() => {
    async function testApi () {
      const { data } = await Axios.get(`${API_URL}/art/${id}`)

      setImage(data)
    }
    testApi()
  }, [id])

  async function handleVote () {
    if (user.votesLeft === 0) {
      setErrorMsg('We are sorry, but you voted already three times')
      return
    }
    const payload = {
      artId: image._id,
      userId: user.id
    }
    try {
      const { data } = await Axios.post(`${API_URL}/vote`, payload)
      if (!data.success) {
        setErrorMsg(data.message)
        setTimeout(() => setErrorMsg(''), 3000)
      } else {
        setImage(data.art)
        setSuccessMsg('Thanks for your vote!')
        setTimeout(() => setSuccessMsg(''), 3000)
        global.localStorage.setItem(
          'user',
          JSON.stringify({ ...user, votesLeft: user.votesLeft - 1 })
        )
      }
    } catch (error) {
      console.error(error)
    }
  }
  return (
    <div>
      <Dimmer active={loading}>
        <Loader />
      </Dimmer>
      <div className='artWrapper'>
        {errorMsg && (
          <Message hidden={!errorMsg} className='floatingMessage' negative>
            <Message.Header>{errorMsg}</Message.Header>
          </Message>
        )}
        {successMsg && (
          <Message hidden={!successMsg} className='floatingMessage' positive>
            <Message.Header>{successMsg}</Message.Header>
          </Message>
        )}
        <Button basic color='blue' onClick={() => history.push('/')}>
          <Icon name='backward' />
          Home
        </Button>
        <Button
          disabled={
            image.voters.includes(user.id) ||
            !votingEnabled ||
            user.votesLeft === 0
          }
          className='float-right'
          color='violet'
          size='medium'
          onClick={handleVote}
        >
          <Icon name='heart' color='red' />
          Vote
        </Button>

        <Header as='h1' dividing>
          {image.title} by {image.copyright}
        </Header>

        <a target='_blank' rel='noopener noreferrer' href={image.artist}>
          {image.artist}
        </a>
        <div className='margin-14-top'>
          <p>{image.desc}</p>
        </div>
        {image.comment && (
          <Message positive>
            <Message.Header>
              This art is video or audio, check it out!
            </Message.Header>
            <a href={image.comment} target='_blank' rel='noopener noreferrer'>
              {image.comment}
            </a>
          </Message>
        )}
        <PrevNextArt currentArtId={id} />
        <Image
          className='margin-14-top margin-14-bottom'
          src={createImageSrc({ path: image.url })}
          srcSet={createImageSrcSet({ path: image.url })}
          wrapped
          onLoad={() => setLoading(false)}
        />
      </div>
      <PrevNextArt currentArtId={id} />
    </div>
  )
}

function PrevNextArt ({ currentArtId }) {
  const history = useHistory()
  const allArt = JSON.parse(global.localStorage.getItem('photos'))
  if (!allArt) {
    return null
  }
  const currentIndex = allArt.indexOf(currentArtId)
  function handlePrevious () {
    history.push(`/art/${allArt[currentIndex - 1]}`)
  }
  function handleNext () {
    history.push(`/art/${allArt[currentIndex + 1]}`)
  }
  return (
    <div style={{ height: 40 }} className='margin-14-top margin-14-bottom'>
      {currentIndex !== 0 && (
        <Button
          onClick={handlePrevious}
          floated='left'
          icon
          labelPosition='right'
        >
          Previous
          <Icon name='left arrow' />
        </Button>
      )}

      {currentIndex !== allArt.length - 1 && (
        <Button onClick={handleNext} floated='right' icon labelPosition='right'>
          Next
          <Icon name='right arrow' />
        </Button>
      )}
    </div>
  )
}
