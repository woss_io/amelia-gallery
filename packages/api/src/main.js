import cors from 'cors'
import dotenv from 'dotenv'
import MongoClient from 'mongodb'
import nanoexpress from 'nanoexpress-pro'
import { prepPhotos } from './prepPhotos.js'
dotenv.config()

async function main () {
  // Connection URL
  const { MONGODB_USER, MONGODB_PWD } = process.env
  // Database Name
  const dbName = 'tcc'
  const url = `mongodb://${MONGODB_USER}:${MONGODB_PWD}@database:27017`
  // const url = `mongodb://${MONGODB_USER}:${MONGODB_PW  D}@127.0.0.1:27017`

  // Use connect method to connect to the server
  const client = await MongoClient.connect(url, {
    useUnifiedTopology: true
  })
  console.log('Connected successfully to server')

  const db = client.db(dbName)
  await db.createCollection('photos')
  await db.createCollection('app_settings')

  // Set up the api
  const app = nanoexpress()

  // app.options('*', cors()) // include before other routes

  // Enable cors
  app.use(cors())

  // const speedLimiter = slowDown({
  //   windowMs: 15 * 60 * 1000, // 15 minutes
  //   delayAfter: 100, // allow 100 requests per 15 minutes, then...
  //   delayMs: 500 // begin adding 500ms of delay per request above 100:
  //   // request # 101 is delayed by  500ms
  //   // request # 102 is delayed by 1000ms
  //   // request # 103 is delayed by 1500ms
  //   // etc.
  // })

  //  Slow down in case of the attack
  // app.use(speedLimiter)

  app.get('/', (req, res) => {
    return res.send({
      message: 'Welcome to Amelia TCC',
      ts: Date.now(),
      status: 'Operational ✔'
    })
  })

  app.get('/photos', async (req, res) => {
    const photos = await db.collection('photos').find()
    return res.send(await photos.toArray())
  })

  app.get('/settings', async (req, res) => {
    const settings = await db
      .collection('app_settings')
      .findOne({ start: true })
    return res.send(await settings)
  })

  app.post('/vote', async (req, res) => {
    const canVote = await db
      .collection('app_settings')
      .findOne({ votingEnabled: true })

    if (!canVote) {
      return res.send({ votingDisabled: true })
    }
    const { artId, userId } = JSON.parse(req.body)

    const coll = db.collection('photos')

    var oId = new MongoClient.ObjectID(artId)
    const mongoArt = await coll.findOne(oId)

    if (mongoArt.voters.includes(userId)) {
      res.send({ success: false, message: 'You cannot vote for the same art' })
    } else {
      const mArt = await coll.replaceOne(
        { _id: oId },
        {
          ...mongoArt,
          votes: mongoArt.votes + 1,
          voters: [...mongoArt.voters, userId]
        }
      )

      const art = {
        ...mArt.ops[0]
      }

      return res.send({ success: true, art })
    }
  })

  app.get('/art/:id', async (req, res) => {
    const { id } = req.params
    var oId = new MongoClient.ObjectID(id)

    const photo = await db.collection('photos').findOne({ _id: oId })

    return res.send(photo)
  })

  app.get('/start', async (req, res) => {
    const started = await db.collection('app_settings').findOne({ start: true })
    if (!started) {
      await db
        .collection('app_settings')
        .insertOne({ start: true, votingEnabled: true, winnerIs: '' })
        .catch()
    }

    return res.send({ started: true })
  })

  app.get('/stats', async (req, res) => {
    const mPhotos = await db
      .collection('photos')
      .find({})
      .sort({ votes: -1 })

    const photosArr = await mPhotos.toArray()
    res.send(photosArr)
  })

  app.post('/choose_winner', async (req, res) => {
    const { artId } = JSON.parse(req.body)

    var oId = new MongoClient.ObjectID(artId)

    const winner = await db
      .collection('photos')
      .findOneAndUpdate(
        { _id: oId },
        { $set: { isWinner: true } },
        { returnNewDocument: true }
      )

    // close the voting
    const settings = await db
      .collection('app_settings')
      .findOneAndUpdate(
        { start: true },
        { $set: { votingEnabled: false, winnerIs: artId } },
        { returnNewDocument: true }
      )

    res.send({ winner: winner.value, settings: settings.value })
  })

  app.get('/process_photos', async (req, res) => {
    const photos = await prepPhotos()
    let message = 'Photos initialized'
    let success = true
    try {
      await db
        .collection('photos')
        .insertMany(photos)
        .catch()

      db.collection('photos').createIndex({ url: 1 }, { unique: true })
    } catch (error) {
      console.error(error)
      message = error
      success = false
    }

    return res.send({
      success,
      message
    })
  })

  app.listen(4567, '0.0.0.0')
}

main().catch(console.error)
