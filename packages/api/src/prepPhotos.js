import ExifReader from 'exifreader'
import { readdirSync, readFileSync } from 'fs'
import { dirname, resolve } from 'path'

const IMGIX_URL = 'https://amelia-tcc.imgix.net'

export async function prepPhotos () {
  const realPath = resolve(dirname(''), './Final')
  const dir = readdirSync(realPath)
  const photosToSave = dir.map(photoFile => {
    const photo = readFileSync(resolve(realPath, photoFile))
    const tags = ExifReader.load(photo)
    // console.log(tags)
    const photoMeta = {
      title: tags.title && tags.title.description,
      artist: tags.Artist && tags.Artist.description,
      copyright: tags.Copyright && tags.Copyright.description,
      desc: tags.ImageDescription && tags.ImageDescription.description,
      url: `${IMGIX_URL}/${photoFile}`,
      comment: tags.UserComment && tags.UserComment.description,
      votes: 0,
      voters: []
    }

    return photoMeta
  })

  return photosToSave
}
